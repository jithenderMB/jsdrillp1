const getSumOfSalariesInEachCountry = candidatesDetails => {

    let result = candidatesDetails.reduce((acc, curr) => {

        let { salary, location } = curr;
        let salaryInInteger = Number(salary.replace(/[$]/g, ''));
        if (acc['Sum of salaries by country']) {

            if (acc['Sum of salaries by country'][location]) {

                acc['Sum of salaries by country'][location] += salaryInInteger;
            }
            else if (location != '') {

                acc['Sum of salaries by country'][location] = salaryInInteger;
            }

        }
        else {

            acc['Sum of salaries by country'] = {};

            acc['Sum of salaries by country'][location] = salaryInInteger;

        }
        
        return acc;
    }, {})

    return result;
};


module.exports = getSumOfSalariesInEachCountry;