const candidates = require("../input");
const getAverageSalaryInEachCountry = require("../averageSalaryInEachCountry");

const result = getAverageSalaryInEachCountry(candidates);

console.log(result);