const candidates = require("../input");
const getSumOfSalariesInEachCountry = require("../sumOfSalariesInEachCountry");

const result = getSumOfSalariesInEachCountry(candidates);

console.log(result);