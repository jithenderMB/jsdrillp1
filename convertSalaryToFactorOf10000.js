const convertSalaryToFactorOf10000 = candidatesDetails => {

    let userArray = [...candidatesDetails];

    userArray.map(candidate => {

        let salary = candidate.salary;

        let salaryInInteger = Number(salary.replace(/[$]/g,''));
        let correctedSalary = salaryInInteger * 10000;

        candidate['salary'] = salaryInInteger;
        candidate['corrected_salary'] = correctedSalary;

    })
    
    return userArray;
};


module.exports = convertSalaryToFactorOf10000;


