const getAverageSalaryInEachCountry = candidatesDetails => {

    let result = candidatesDetails.reduce((acc, curr) => {

        let { salary, location } = curr;
        let salaryInInteger = Number(salary.replace(/[$]/g, ''));
        if (acc[location]) {

            acc[location]['salary'] += salaryInInteger;

            acc[location]['count'] += 1;
        }
        else {

            acc[location] = {};

            acc[location] = { 'salary': salaryInInteger, 'count': 1 };

        }

        return acc;
    }, {})

    Object.keys(result).map(country => {
       
        let average = result[country].salary / result[country].count;
        
        result[country] = Number(average.toFixed(2));
    });
    
    return result;
};


module.exports = getAverageSalaryInEachCountry;