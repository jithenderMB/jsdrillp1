const getAllWebDeveloper = candidatesDetails => {
    
    let result = candidatesDetails.filter(candidate => {
        let profession = candidate.job;
        return  /Web Developer/.test(profession);
    })
    return result;
};


module.exports = getAllWebDeveloper;