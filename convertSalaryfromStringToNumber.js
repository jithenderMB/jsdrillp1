const getSalaryInNumbers = candidatesDetails => {

    let userArray = [...candidatesDetails];

    userArray.map(candidate => {

        let salary = candidate.salary;
        let salaryInInteger = Number(salary.replace(/[$]/g,''));
        candidate['salary'] = salaryInInteger;

    })
    
    return userArray;
};


module.exports = getSalaryInNumbers;


