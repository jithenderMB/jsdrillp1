const getSumOfAllSalaries = candidatesDetails => {

    let sumOfSalaries = 0;

    candidatesDetails.map(candidate => {
        
        let salary = candidate.salary;
        let salaryInInteger = Number(salary.replace(/[$]/g,''));
        sumOfSalaries += salaryInInteger;

    })
    
    return sumOfSalaries.toFixed(2);
};


module.exports = getSumOfAllSalaries;
